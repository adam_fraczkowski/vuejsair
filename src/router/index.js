import Vue from 'vue'
import Router from 'vue-router'
import StatsComponent from '@/components/StatsComponent'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'StatsComponent',
      component: StatsComponent
    },
    {
      path: '/devs/:station',
      name: 'StatsComponentDev',
      component: StatsComponent
    }
  ]
})
